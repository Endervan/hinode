
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>

</head>
<body class="bg-fornecedores">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->


    <!--CABECALHO FORNECEDORES -->
    <!--  ==============================================================  -->
    <div class="container bg-descricao-empresa">
        <div class="row">
            <div class="col-xs-7">
                <h3 class="top20">CONFIRA NOSSOS <span>FORNECEDORES</span></h3>
            </div>
        </div>
    </div>
    <!-- CABECALHO FORNECEDORES -->
    <!--  ==============================================================  -->




    <!-- NOSSO FORNECEDORES -->
    <!--  ==============================================================  -->
    <div class="container fundo-branco-fornecedores pb100">
        <div class="row">
            <div class="col-xs-12 bottom50">
                <p>Lorem Ipsum is simply dummy text of the printing</p>
            </div>

            <!-- dicas 01 -->
            <!--  ==============================================================  -->
            <div class="col-xs-4 fornecedores bottom30">
              <a class="thumbnail top10" href="">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/fornecedores01.jpg" alt="" class="input100">
              </a>
              <!-- descricao produtos-->
              <div class="col-xs-12">
                <h1>Aqui fica o conteúdo digitado pelo usuaário Aqui fica o conteúdo digitado pelo usuaário</h1>
                <h2 class"top10">www.orempsum.com.br</h2>
                <p class"top10">(61) 3322-1100 (61) 3322-1100</p>
            </div>
            <!-- descricao produtos-->
        </div>
        <!-- dicas 01 -->
        <!--  ==============================================================  -->

        <!-- dicas 02 -->
            <!--  ==============================================================  -->
            <div class="col-xs-4 fornecedores bottom30">
              <a class="thumbnail top10" href="">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/fornecedores02.jpg" alt="" class="input100">
              </a>
              <!-- descricao produtos-->
              <div class="col-xs-12">
                <h1>Aqui fica o conteúdo digitado pelo usuaário Aqui fica o conteúdo digitado pelo usuaário</h1>
                <h2 class"top10">www.orempsum.com.br</h2>
                <p class"top10">(61) 3322-1100 (61) 3322-1100</p>
            </div>
            <!-- descricao produtos-->
        </div>
        <!-- dicas 02 -->
        <!--  ==============================================================  -->

        <!-- dicas 03 -->
            <!--  ==============================================================  -->
            <div class="col-xs-4 fornecedores bottom30">
              <a class="thumbnail top10" href="">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/fornecedores03.jpg" alt="" class="input100">
              </a>
              <!-- descricao produtos-->
              <div class="col-xs-12">
                <h1>Aqui fica o conteúdo digitado pelo usuaário Aqui fica o conteúdo digitado pelo usuaário</h1>
                <h2 class"top10">www.orempsum.com.br</h2>
                <p class"top10">(61) 3322-1100 (61) 3322-1100</p>
            </div>
            <!-- descricao produtos-->
        </div>
        <!-- dicas 03 -->
        <!--  ==============================================================  -->

         <!-- dicas 04 -->
            <!--  ==============================================================  -->
            <div class="col-xs-4 fornecedores bottom30">
              <a class="thumbnail top10" href="">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/fornecedores01.jpg" alt="" class="input100">
              </a>
              <!-- descricao produtos-->
              <div class="col-xs-12">
                <h1>Aqui fica o conteúdo digitado pelo usuaário Aqui fica o conteúdo digitado pelo usuaário</h1>
                <h2 class"top10">www.orempsum.com.br</h2>
                <p class"top10">(61) 3322-1100 (61) 3322-1100</p>
            </div>
            <!-- descricao produtos-->
        </div>
        <!-- dicas 04 -->
        <!--  ==============================================================  -->

        <!-- dicas 05 -->
            <!--  ==============================================================  -->
            <div class="col-xs-4 fornecedores bottom30">
              <a class="thumbnail top10" href="">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/fornecedores02.jpg" alt="" class="input100">
              </a>
              <!-- descricao produtos-->
              <div class="col-xs-12">
                <h1>Aqui fica o conteúdo digitado pelo usuaário Aqui fica o conteúdo digitado pelo usuaário</h1>
                <h2 class"top10">www.orempsum.com.br</h2>
                <p class"top10">(61) 3322-1100 (61) 3322-1100</p>
            </div>
            <!-- descricao produtos-->
        </div>
        <!-- dicas 05 -->
        <!--  ==============================================================  -->

        <!-- dicas 06 -->
            <!--  ==============================================================  -->
            <div class="col-xs-4 fornecedores bottom30">
              <a class="thumbnail top10" href="">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/fornecedores03.jpg" alt="" class="input100">
              </a>
              <!-- descricao produtos-->
              <div class="col-xs-12">
                <h1>Aqui fica o conteúdo digitado pelo usuaário Aqui fica o conteúdo digitado pelo usuaário</h1>
                <h2 class"top10">www.orempsum.com.br</h2>
                <p class"top10">(61) 3322-1100 (61) 3322-1100</p>
            </div>
            <!-- descricao produtos-->
        </div>
        <!-- dicas 06 -->
        <!--  ==============================================================  -->

        <!-- dicas 07 -->
            <!--  ==============================================================  -->
            <div class="col-xs-4 fornecedores bottom30">
              <a class="thumbnail top10" href="">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/fornecedores01.jpg" alt="" class="input100">
              </a>
              <!-- descricao produtos-->
              <div class="col-xs-12">
                <h1>Aqui fica o conteúdo digitado pelo usuaário Aqui fica o conteúdo digitado pelo usuaário</h1>
                <h2 class"top10">www.orempsum.com.br</h2>
                <p class"top10">(61) 3322-1100 (61) 3322-1100</p>
            </div>
            <!-- descricao produtos-->
        </div>
        <!-- dicas 07 -->
        <!--  ==============================================================  -->

        <!-- dicas 08 -->
            <!--  ==============================================================  -->
            <div class="col-xs-4 fornecedores bottom30">
              <a class="thumbnail top10" href="">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/fornecedores02.jpg" alt="" class="input100">
              </a>
              <!-- descricao produtos-->
              <div class="col-xs-12">
                <h1>Aqui fica o conteúdo digitado pelo usuaário Aqui fica o conteúdo digitado pelo usuaário</h1>
                <h2 class"top10">www.orempsum.com.br</h2>
                <p class"top10">(61) 3322-1100 (61) 3322-1100</p>
            </div>
            <!-- descricao produtos-->
        </div>
        <!-- dicas 08 -->
        <!--  ==============================================================  -->

        <!-- dicas 09 -->
            <!--  ==============================================================  -->
            <div class="col-xs-4 fornecedores bottom30">
              <a class="thumbnail top10" href="">
                  <img  src="<?php echo Util::caminho_projeto() ?>/imgs/fornecedores03.jpg" alt="" class="input100">
              </a>
              <!-- descricao produtos-->
              <div class="col-xs-12">
                <h1>Aqui fica o conteúdo digitado pelo usuaário Aqui fica o conteúdo digitado pelo usuaário</h1>
                <h2 class"top10">www.orempsum.com.br</h2>
                <p class"top10">(61) 3322-1100 (61) 3322-1100</p>
            </div>
            <!-- descricao produtos-->
        </div>
        <!-- dicas 09 -->
        <!--  ==============================================================  -->

    </div>
</div>
<!-- NOSSO FORNECEDORES -->
<!--  ==============================================================  -->


<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>


