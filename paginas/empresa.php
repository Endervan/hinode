
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>

</head>
<body class="bg-empresa">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->


    <!--CABECALHO EMPRESA -->
    <!--  ==============================================================  -->
    <div class="container bg-descricao-empresa">
        <div class="row">
            <div class="col-xs-7">
                <h3 class="top20">CONHEÇA NOSSA <span>EMPRESA</span></h3>
                
            </div>
        </div>
    </div>
    <!-- CABECALHO EMPRESA -->
    <!--  ==============================================================  -->




    <!-- DESCRICAO EMPRESA -->
    <!--  ==============================================================  -->
    <div class="container bg-descricao fundo-branco-empresa">
        <div class="row">
            <div class="col-xs-7">
                <p class="top10">Esquadrias de alumínio, a junção de tradição e qualidade.</p>
            </div>
            <div class="col-xs-12">
                <p>A ALUTEC - DF, com 19 anos de tradição no mercado, tem como principal atividade a fabricação
                    e instalação de esquadrias de alumínio sob medida para construção civil, desde obras convencionais
                    até as mais sofisticadas e complexas. Valorizando sempre o projeto do cliente, oferece as mais ade
                    quadas soluções para obras residenciais, comerciais e de grande porte.
                </p>
                <h3 class="top30 right20">ATENDIMENTO PERSONALIZADO</h3>
                <p class="top30">Com o mercado competitivo cada dia mais exigente, a ALUTEC - DF se destaca com seu atendimen
                    to personalizado no que abrange a esquadrias de alumínio sob medida. Desenvolvidas por profissio
                    nais altamente qualificados, garantindo um elevado padrão de qualidade, tempo otimizado e custo 
                    reduzido. A empresa também oferece a seus clientes orientação técnica para melhor aproveitamen
                    to de modelos e medidas, para assim valorizar as condições de seu ambiente.
                </p>
                <h2 class="top30 right20">ATENDIMENTO PERSONALIZADO</h2>
                <p class="top25">Utilizamos sempre materiais dos melhores fornecedores, proporcionando conforto, segurança, quali
                    dade, instalação com rapidez e durabilidade.
                </p>
                <p class="top25 bottom100">Trabalhamos com linhas de última geração, linhas próprias e em parcerias com empresas líderes.</p>
            </div>
        </div>
    </div>
    <!-- DESCRICAO EMPRESA -->
    <!--  ==============================================================  -->




    <!-- maps -->
    <!--  ==============================================================  -->
    <div class="container mapa">
        <div class="row">
            <div class="col-xs-12 descricao-mapa">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/saiba-como-chegar.png" alt="">
                <h1 class="pull-right top15">ATENDIMENTO:<span>(61) 3404-5828</span></h1>
            </div>
            <div class="mapa-posicao">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3839.244790325365!2d-47.99118844926741!3d-15.791041388999558!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3036a1f71519%3A0x27b92164c0a89908!2sAlutec+esquadrias+de+aluminio!5e0!3m2!1spt-BR!2sbr!4v1449517142913" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <!-- maps -->
    <!--  ==============================================================  -->

    <!-- rodape -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- rodape -->

</body>
</html>


