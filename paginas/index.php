<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>


    <!-- Jumbo-Slider-2spaltig -->
    <script type="text/javascript">
      jQuery(document).ready(function($) {

        $('#myCarousel').carousel({
            interval: 4000,
        });

        $('#carousel-text').html($('#slide-content-0').html());


        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
            var id = $('.item.active').data('slide-number');
            $('#carousel-text').html($('#slide-content-'+id).html());
        });
    });
  </script>
  <!-- Jumbo-Slider-2spaltig -->


</head>



<body>



 <!--  ==============================================================  -->
 <!-- conteudo -->
 <div class="pagina">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->
</div>
<!-- conteudo -->
<!--  ==============================================================  -->




<!-- ==============================================================  -->
<!--  BANNERS  HOME -->


<!-- Jumbo-Slider-2spaltig -->

<div class="container-fluid">
    <!-- Slider -->
    <div class="row">
        <div class="carroucel-descricao" id="slider">
            <!-- Top part of the slider -->
            <div id="carousel-bounding-box">
                <div class="carousel slide" id="myCarousel" data-ride="carousel">

                    <!-- Carousel items -->
                    <div class="carousel-inner bordered">
                        <div class="item active" data-slide-number="0">
                            <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-home01.jpg" alt="">
                        </div>

                        <div class="item" data-slide-number="1">
                            <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-home02.jpg" alt="">
                        </div>

                        <div class="item" data-slide-number="2">
                            <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-home03.jpg" alt="">
                        </div>

                    </div>
                    <!-- Carousel nav -->



                </div>
            </div>



            <!-- conteudo do bg home -->
            <div class="container descricao-bg-home">
                <div class="row">

                    <div class="col-xs-10 col-xs-offset-1 fundo-degrader posicao" id="carousel-text"></div>
                    <a class="carousel-control left" data-slide="prev" href="#myCarousel"><i class="fa fa-angle-left fa-2x"></i></a> 
                    <a class="carousel-control right" data-slide="next" href="#myCarousel"><i class="fa fa-angle-right fa-2x"></i></a>

                    <div id="slide-content" style="display: none;">

                        <div id="slide-content-0">
                            <div class="col-xs-8">
                                <h1 class="top20">LINHA DE <span>MAQUIAGEM 01</span></h1>
                            </div>
                            <div class="col-xs-4">
                                <h2 class="top35 pb20">FAÇA SEU ORÇAMENTO</h2>
                            </div>
                            <div class="col-xs-12">
                                <p class="top50">  Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard
                                    ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to
                                </p>
                                <a href="#" class="btn btn-saiba-mais top30">SAIBA MAIS<i class="fa fa-chevron-right"></i></i></a>
                            </div>
                        </div>

                        <div id="slide-content-1">
                            <div class="col-xs-8">
                                <h1 class="top20">LINHA DE <span>MAQUIAGEM 02</span></h1>
                            </div>
                            <div class="col-xs-4">
                                <h2 class="top35 pb20">FAÇA SEU ORÇAMENTO</h2>
                            </div>
                            <div class="col-xs-12">
                                <p class="top50">  Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard
                                    ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to
                                </p>
                                <a href="#" class="btn btn-saiba-mais top30">SAIBA MAIS<i class="fa fa-chevron-right"></i></i></a>
                            </div>
                        </div>

                        <div id="slide-content-2">
                            <div class="col-xs-8">
                                <h1 class="top20">LINHA DE <span>MAQUIAGEM 03</span></h1>
                            </div>
                            <div class="col-xs-4">
                                <h2 class="top35 pb20">FAÇA SEU ORÇAMENTO</h2>
                            </div>
                            <div class="col-xs-12">
                                <p class="top50">  Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum has been the industry's standard
                                    ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to
                                </p>
                                <a href="#" class="btn btn-saiba-mais top30">SAIBA MAIS<i class="fa fa-chevron-right"></i></i></a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <!-- conteudo do bg home -->

        </div>
        <!--/Slider-->
    </div>
</div>

<!-- Jumbo-Slider-2spaltig -->

<!--  BANNERS  HOME -->
<!--  ==============================================================  -->

<!--  ==============================================================  -->
<!-- PRODUTOS HOME -->
<!--  ==============================================================  -->
<div class="container top60 bottom50">
    <div class="row">
        <!-- produto 01 -->
        <div class="col-xs-4 listagem-produtos text-center">
            <div class="thumbnail effect6">
                <a href="" title="Saiba mais">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.png" alt="" class="input100">
              </a>
              <div class="caption">
                <h1 class="top30">Maquiagem</h1>
                <p class="top20">Lorem Ipsum is simply dummy text
                   of the printing and typesetting
                   industry. 
               </p>
           </div>
       </div> 
   </div>
   <!-- produto 01 -->

   <!-- produto 02 -->
        <div class="col-xs-4 listagem-produtos text-center">
            <div class="thumbnail effect6">
                <a href="" title="Saiba mais">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home02.png" alt="" class="input100">
              </a>
              <div class="caption">
                <h1 class="top30">Cabelos</h1>
                <p class="top20">Lorem Ipsum is simply dummy text
                   of the printing and typesetting
                   industry. 
               </p>
           </div>
       </div> 
   </div>
   <!-- produto 02 -->

   <!-- produto 03 -->
        <div class="col-xs-4 listagem-produtos text-center">
            <div class="thumbnail effect6">
                <a href="" title="Saiba mais">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home03.png" alt="" class="input100">
              </a>
              <div class="caption">
                <h1 class="top30">Bem-estar</h1>
                <p class="top20">Lorem Ipsum is simply dummy text
                   of the printing and typesetting
                   industry. 
               </p>
           </div>
       </div> 
   </div>
   <!-- produto 03 -->

</div>
</div>


<!--  ==============================================================  -->
<!-- PRODUTOS HOME -->
<!--  ==============================================================  -->





<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->





</body>

</html>
