
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>

</head>
<body class="bg-dicas">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->

    <!--CABECALHO DICAS -->
    <!--  ==============================================================  -->
    <div class="container bg-descricao-dicas">
        <div class="row">
            <div class="col-xs-7">
                <h3 class="top20">CONFIRA NOSSAS <span>DICA</span></h3>
                <p class="top10">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
        </div>
    </div>
    <!-- CABECALHO DICAS -->
    <!--  ==============================================================  -->


    <!-- DESCRICAO DICAS DENTRO -->
    <!--  ==============================================================  -->
    <div class="container fundo-branco-dicas pb200">
        <div class="row">
            <div class="col-xs-8">
                <h1>THIS IS A STANDARD POSTWITH A PREVIEW IMAGE</h1>
                <h2 class="top10 bottom20">10/10/2015</h2>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-dentro-banner.jpg" alt="">
                <p class="top30">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                    has been the industry's standard dummy text ever since the 1500s, when an unknown pr
                    inter took a galley of type and scrambled it to make a type specimen book. It has survive
                    d not only five centuries, but also the leap into electronic typesetting, remaining essential
                    ly unchanged. It was popularised in the 1960s with the release of Letraset sheets contain
                    ing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                    PageMaker including versions of Lorem Ipsum.
                </p>
                <p class="top50">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                    has been the industry's standard dummy text ever since the 1500s, when an unknown pr
                    inter took a galley of type and scrambled it to make a type specimen book. It has survive
                    d not only five centuries, but also the leap into electronic typesetting, remaining essential
                    ly unchanged. It was popularised in the 1960s with the release of Letraset sheets contain
                    ing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                    PageMaker including versions of Lorem Ipsum.
                </p>
            </div>
        </div>
    </div>
    <!-- DESCRICAO DICAS DENTRO -->
    <!--  ==============================================================  -->



    <!-- rodape -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- rodape -->

</body>
</html>


