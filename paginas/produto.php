
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?>

</head>
<body class="bg-produtos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->


  <!--CABECALHO PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container bg-descricao-produto">
    <div class="row">
      <div class="col-xs-7">
        <h3 class="top20">CONFIRA NOSSOS <span>PRODUTOS</span></h3>
      </div>
    </div>
  </div>
  <!-- CABECALHO PRODUTOS -->
  <!--  ==============================================================  -->




  <!-- NOSSO PRODUTOS DENTRO -->
  <!--  ==============================================================  -->
  <div class="container fundo-degarder-produtos pb100">
    <div class="row seta">
      <div class="col-xs-12 bottom50">
        <p class="top20">Lorem Ipsum is simply dummy text of the printing</p>
        <h1>REVESTIMENTO ACM</h1>
        <div class="col-xs-8">
          <p class="top30">A ALUTEC - DF, com 19 anos de tradição no mercado, tem como principal a
            tividade a fabricaçãoe instalação de esquadrias de alumínio sob medida para
            construção civil, desde obras convencionaisaté as mais sofisticadas e compl
            exas. Valorizando sempre o projeto do cliente, oferece as mais adequadas so
            luções para obras residenciais, comerciais e de grande porte.
          </p>
          <!-- DEPOIMENTO -->
          <div class="col-xs-3 text-center top115">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/oquedizem.png" alt="">
          </div>
          <div class="col-xs-9 text-center top115">
            <p>A ALUTEC - DF, com 19 anos de tradição no mercado, tem como principal a
              tividade a fabricaçãoe instalação de esquadrias de alumínio sob medida para
              construção civil, desde obras convencionaisaté as mais sofisticadas e compl
              exas. Valorizando sempre o projeto do cliente, oferece as mais adequadas so
              luções para obras residenciais, comerciais e de grande porte.
            </p>
            <h3 class="top15">SARAH SOUSA</h3>
          </div>
          <!-- DEPOIMENTO -->
        </div>
      </div>



    </div>
  </div>
  <!-- NOSSO PRODUTOS DENTRO -->
  <!--  ==============================================================  -->


  <!-- PRODUTO HOME -->
  <!--  ==============================================================  -->
  <div class="container fundo-branco seta  subir pb60">
    <div class="row">
    <div class="veja"></div>
      <div class='col-xs-12 carroucel-premiadas'>
        <div class="carousel slide media-carousel" id="media">

          <!-- Indicators -->
          <ol class="carousel-indicators navs-carroucel top100">
            <li data-target="#media" data-slide-to="0" class="active"></li>
            <li data-target="#media" data-slide-to="1"></li>
            <li data-target="#media" data-slide-to="2"></li>
          </ol>

          <div class="carousel-inner">

            <div class="item  active">
              <div class="row top60">
                <!-- produto 01 -->
                <!--  ==============================================================  -->
                <div class="col-xs-6">
                  <a class="thumbnail top30" href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
                  </a>
                  <!-- descricao produtos-->
                  <div class="col-xs-8">
                    <P>
                      Fachadas em Pele de Vidro são muito utiliza
                      das em empreendimentos comerciais e resi
                      denciais com grande estilo. 
                    </P>
                  </div>
                  <!-- descricao produtos-->

                  <!-- BOTAO SAIBA MAIS-->
                  <div class="col-xs-4">
                    <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
                  </div>
                  <!-- BOTAO SAIBA MAIS-->
                </div>
                <!-- produto 01 -->
                <!--  ==============================================================  -->


                <!-- produto 02 -->
                <!--  ==============================================================  -->
                <div class="col-xs-6">
                  <a class="thumbnail top30" href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
                  </a>
                  <!-- descricao produtos-->
                  <div class="col-xs-8">
                    <P>
                      Fachadas em Pele de Vidro são muito utiliza
                      das em empreendimentos comerciais e resi
                      denciais com grande estilo. 
                    </P>
                  </div>
                  <!-- descricao produtos-->

                  <!-- BOTAO SAIBA MAIS-->
                  <div class="col-xs-4">
                    <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
                  </div>
                  <!-- BOTAO SAIBA MAIS-->
                </div>
                <!-- produto 02 -->
                <!--  ==============================================================  -->

              </div>
            </div>

            <div class="item">
              <div class="row top60">
                <!-- produto 03-->
                <!--  ==============================================================  -->
                <div class="col-xs-6">
                  <a class="thumbnail top30" href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
                  </a>
                  <!-- descricao produtos-->
                  <div class="col-xs-8">
                    <P>
                      Fachadas em Pele de Vidro são muito utiliza
                      das em empreendimentos comerciais e resi
                      denciais com grande estilo. 
                    </P>
                  </div>
                  <!-- descricao produtos-->

                  <!-- BOTAO SAIBA MAIS-->
                  <div class="col-xs-4">
                    <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
                  </div>
                  <!-- BOTAO SAIBA MAIS-->
                </div>
                <!-- produto 03 -->
                <!--  ==============================================================  -->


                <!-- produto 04 -->
                <!--  ==============================================================  -->
                <div class="col-xs-6">
                  <a class="thumbnail top30" href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
                  </a>
                  <!-- descricao produtos-->
                  <div class="col-xs-8">
                    <P>
                      Fachadas em Pele de Vidro são muito utiliza
                      das em empreendimentos comerciais e resi
                      denciais com grande estilo. 
                    </P>
                  </div>
                  <!-- descricao produtos-->

                  <!-- BOTAO SAIBA MAIS-->
                  <div class="col-xs-4">
                    <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
                  </div>
                  <!-- BOTAO SAIBA MAIS-->
                </div>
                <!-- produto 04 -->
                <!--  ==============================================================  -->

                
              </div>
            </div>

            <div class="item">
              <div class="row top60">
                <!-- produto 05 -->
                <!--  ==============================================================  -->
                <div class="col-xs-6">
                  <a class="thumbnail top30" href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
                  </a>
                  <!-- descricao produtos-->
                  <div class="col-xs-8">
                    <P>
                      Fachadas em Pele de Vidro são muito utiliza
                      das em empreendimentos comerciais e resi
                      denciais com grande estilo. 
                    </P>
                  </div>
                  <!-- descricao produtos-->

                  <!-- BOTAO SAIBA MAIS-->
                  <div class="col-xs-4">
                    <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
                  </div>
                  <!-- BOTAO SAIBA MAIS-->
                </div>
                <!-- produto 05 -->
                <!--  ==============================================================  -->


                <!-- produto 06 -->
                <!--  ==============================================================  -->
                <div class="col-xs-6">
                  <a class="thumbnail top30" href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
                  </a>
                  <!-- descricao produtos-->
                  <div class="col-xs-8">
                    <P>
                      Fachadas em Pele de Vidro são muito utiliza
                      das em empreendimentos comerciais e resi
                      denciais com grande estilo. 
                    </P>
                  </div>
                  <!-- descricao produtos-->

                  <!-- BOTAO SAIBA MAIS-->
                  <div class="col-xs-4">
                    <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
                  </div>
                  <!-- BOTAO SAIBA MAIS-->
                </div>
                <!-- produto 06 -->
                <!--  ==============================================================  -->

              </div>
            </div>

          </div>
          <a data-slide="prev" href="#media" class="left carousel-control"><i class="fa fa-angle-left fa-2x"></i></a>
          <a data-slide="next" href="#media" class="right carousel-control"><i class="fa fa-angle-right fa-2x"></i></a>
        </div>                          
      </div>

    </div>
  </div>

  <!-- PRODUTO HOME -->
  <!--  ==============================================================  -->



  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->

</body>
</html>



