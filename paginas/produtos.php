
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- Media Slider Carousel BS3   -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('#media').carousel({
        pause: true,
        interval:3000,
    });
  });</script>
  <!-- Media Slider Carousel BS3   -->

</head>
<body class="bg-produtos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->


  <!--CABECALHO PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container bg-descricao-empresa">
    <div class="row">
      <div class="col-xs-7">
        <h3 class="top20">CONFIRA NOSSOS <span>PRODUTOS</span></h3>
      </div>
    </div>
  </div>
  <!-- CABECALHO PRODUTOS -->
  <!--  ==============================================================  -->




  <!-- NOSSO PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container fundo-branco-fornecedores pb100">
  <div class="row carroucel-premiadas seta">
      <div class="col-xs-12 bottom50">
        <p>Lorem Ipsum is simply dummy text of the printing</p>
      </div>

      <!-- produto 01 -->
      <!--  ==============================================================  -->
      <div class="col-xs-6">
        <a class="thumbnail top30" href="<?php echo Util::caminho_projeto() ?>/produto">
          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
        </a>
        <!-- descricao produtos-->
        <div class="col-xs-8">
          <P>
            Fachadas em Pele de Vidro são muito utiliza
            das em empreendimentos comerciais e resi
            denciais com grande estilo. 
          </P>
        </div>
        <!-- descricao produtos-->

        <!-- BOTAO SAIBA MAIS-->
        <div class="col-xs-4">
          <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
        </div>
        <!-- BOTAO SAIBA MAIS-->
      </div>
      <!-- produto 01 -->
      <!--  ==============================================================  -->


      <!-- produto 02 -->
      <!--  ==============================================================  -->
      <div class="col-xs-6">
        <a class="thumbnail top30" href="<?php echo Util::caminho_projeto() ?>/produto">
          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
        </a>
        <!-- descricao produtos-->
        <div class="col-xs-8">
          <P>
            Fachadas em Pele de Vidro são muito utiliza
            das em empreendimentos comerciais e resi
            denciais com grande estilo. 
          </P>
        </div>
        <!-- descricao produtos-->

        <!-- BOTAO SAIBA MAIS-->
        <div class="col-xs-4">
          <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
        </div>
        <!-- BOTAO SAIBA MAIS-->
      </div>
      <!-- produto 02 -->
      <!--  ==============================================================  -->

      <!-- produto 03 -->
      <!--  ==============================================================  -->
      <div class="col-xs-6">
        <a class="thumbnail top30" href="<?php echo Util::caminho_projeto() ?>/produto">
          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
        </a>
        <!-- descricao produtos-->
        <div class="col-xs-8">
          <P>
            Fachadas em Pele de Vidro são muito utiliza
            das em empreendimentos comerciais e resi
            denciais com grande estilo. 
          </P>
        </div>
        <!-- descricao produtos-->

        <!-- BOTAO SAIBA MAIS-->
        <div class="col-xs-4">
          <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
        </div>
        <!-- BOTAO SAIBA MAIS-->
      </div>
      <!-- produto 03 -->
      <!--  ==============================================================  -->


      <!-- produto 04 -->
      <!--  ==============================================================  -->
      <div class="col-xs-6">
        <a class="thumbnail top30" href="<?php echo Util::caminho_projeto() ?>/produto">
          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
        </a>
        <!-- descricao produtos-->
        <div class="col-xs-8">
          <P>
            Fachadas em Pele de Vidro são muito utiliza
            das em empreendimentos comerciais e resi
            denciais com grande estilo. 
          </P>
        </div>
        <!-- descricao produtos-->

        <!-- BOTAO SAIBA MAIS-->
        <div class="col-xs-4">
          <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
        </div>
        <!-- BOTAO SAIBA MAIS-->
      </div>
      <!-- produto 04 -->
      <!--  ==============================================================  -->

      <!-- produto 05 -->
      <!--  ==============================================================  -->
      <div class="col-xs-6">
        <a class="thumbnail top30" href="<?php echo Util::caminho_projeto() ?>/produto">
          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
        </a>
        <!-- descricao produtos-->
        <div class="col-xs-8">
          <P>
            Fachadas em Pele de Vidro são muito utiliza
            das em empreendimentos comerciais e resi
            denciais com grande estilo. 
          </P>
        </div>
        <!-- descricao produtos-->

        <!-- BOTAO SAIBA MAIS-->
        <div class="col-xs-4">
          <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
        </div>
        <!-- BOTAO SAIBA MAIS-->
      </div>
      <!-- produto 05 -->
      <!--  ==============================================================  -->


      <!-- produto 06 -->
      <!--  ==============================================================  -->
      <div class="col-xs-6">
        <a class="thumbnail top30" href="<?php echo Util::caminho_projeto() ?>/produto">
          <img  src="<?php echo Util::caminho_projeto() ?>/imgs/produto-home01.jpg" alt="">
        </a>
        <!-- descricao produtos-->
        <div class="col-xs-8">
          <P>
            Fachadas em Pele de Vidro são muito utiliza
            das em empreendimentos comerciais e resi
            denciais com grande estilo. 
          </P>
        </div>
        <!-- descricao produtos-->

        <!-- BOTAO SAIBA MAIS-->
        <div class="col-xs-4">
          <a href="#" class="btn btn-saiba-mais pull-right">SAIBA MAIS</a>
        </div>
        <!-- BOTAO SAIBA MAIS-->
      </div>
      <!-- produto 06 -->
      <!--  ==============================================================  -->


    </div>
  </div>
  <!-- NOSSO PRODUTOS -->
  <!--  ==============================================================  -->


  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->

</body>
</html>



    