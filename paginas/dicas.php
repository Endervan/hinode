
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>

</head>
<body class="bg-dicas">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->

    <!--CABECALHO DICAS -->
    <!--  ==============================================================  -->
    <div class="container bg-descricao-dicas">
        <div class="row">
            <div class="col-xs-7">
                <h3 class="top20">CONFIRA NOSSAS <span>DICAS</span></h3>
                <p class="top10">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                </p>
            </div>
        </div>
    </div>
    <!-- CABECALHO DICAS -->
    <!--  ==============================================================  -->


    <!-- DESCRICAO DICAS -->
    <!--  ==============================================================  -->

    <div class="container fundo-branco-dicas pb50">
        <div class="row  carroucel-premiadas1 seta1">

                <!-- dicas 01 -->
                <!--  ==============================================================  -->
                <div class="col-xs-3">
                  <div class="colocacao-icon">
                      <a class="thumbnail top20 effect2 " href="<?php echo Util::caminho_projeto() ?>/dica">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-home01.jpg" alt="" class="input100">
                    </a>
                    <a class="icon-saiba" href="#">
                        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/icon-saiba-mais.png" alt="">
                    </a>
                </div>
                <!-- descricao produtos-->
                <div class="col-xs-12">
                    <P>
                        ALUTEC -DF se destaca com se
                        ge a esquadrias de alumínio sob 
                        te qualificados, garantindo um 
                        custo reduzido.
                    </P>
                </div>
                <!-- descricao produtos-->

            </div>
            <!-- dicas 01 -->
            <!--  ==============================================================  -->

            <!-- dicas 02-->
            <!--  ==============================================================  -->
            <div class="col-xs-3">
              <div class="colocacao-icon">
                  <a class="thumbnail top20 effect2 " href="<?php echo Util::caminho_projeto() ?>/dica">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-home01.jpg" alt="" class="input100">
                </a>
                <a class="icon-saiba" href="#">
                    <img  src="<?php echo Util::caminho_projeto() ?>/imgs/icon-saiba-mais.png" alt="">
                </a>
            </div>
            <!-- descricao produtos-->
            <div class="col-xs-12">
                <P>
                    ALUTEC -DF se destaca com se
                    ge a esquadrias de alumínio sob 
                    te qualificados, garantindo um 
                    custo reduzido.
                </P>
            </div>
            <!-- descricao produtos-->

        </div>
        <!-- dicas 02-->
        <!--  ==============================================================  -->

        <!-- dicas 03 -->
        <!--  ==============================================================  -->
        <div class="col-xs-3">
          <div class="colocacao-icon">
              <a class="thumbnail top20 effect2 " href="<?php echo Util::caminho_projeto() ?>/dica">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-home01.jpg" alt="" class="input100">
            </a>
            <a class="icon-saiba" href="#">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/icon-saiba-mais.png" alt="">
            </a>
        </div>
        <!-- descricao produtos-->
        <div class="col-xs-12">
            <P>
                ALUTEC -DF se destaca com se
                ge a esquadrias de alumínio sob 
                te qualificados, garantindo um 
                custo reduzido.
            </P>
        </div>
        <!-- descricao produtos-->

        </div>
        <!-- dicas 03 -->
        <!--  ==============================================================  -->

        <div class="clearfix"></div>





        <!-- dicas 04 -->
        <!--  ==============================================================  -->
        <div class="col-xs-3">
          <div class="colocacao-icon">
              <a class="thumbnail top20 effect2 " href="<?php echo Util::caminho_projeto() ?>/dica">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-home01.jpg" alt="" class="input100">
            </a>
            <a class="icon-saiba" href="#">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/icon-saiba-mais.png" alt="">
            </a>
        </div>
        <!-- descricao produtos-->
        <div class="col-xs-12">
            <P>
                ALUTEC -DF se destaca com se
                ge a esquadrias de alumínio sob 
                te qualificados, garantindo um 
                custo reduzido.
            </P>
        </div>
        <!-- descricao produtos-->

        </div>
        <!-- dicas 04 -->
        <!--  ==============================================================  -->

        <!-- dicas 05-->
        <!--  ==============================================================  -->
        <div class="col-xs-3">
          <div class="colocacao-icon">
              <a class="thumbnail top20 effect2 " href="<?php echo Util::caminho_projeto() ?>/dica">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-home01.jpg" alt="" class="input100">
            </a>
            <a class="icon-saiba" href="#">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/icon-saiba-mais.png" alt="">
            </a>
        </div>
        <!-- descricao produtos-->
        <div class="col-xs-12">
            <P>
                ALUTEC -DF se destaca com se
                ge a esquadrias de alumínio sob 
                te qualificados, garantindo um 
                custo reduzido.
            </P>
        </div>
        <!-- descricao produtos-->

        </div>
        <!-- dicas 05-->
        <!--  ==============================================================  -->

        <!-- dicas 06 -->
        <!--  ==============================================================  -->
        <div class="col-xs-3">
          <div class="colocacao-icon">
              <a class="thumbnail top20 effect2 " href="<?php echo Util::caminho_projeto() ?>/dica">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-home01.jpg" alt="" class="input100">
            </a>
            <a class="icon-saiba" href="#">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/icon-saiba-mais.png" alt="">
            </a>
        </div>
        <!-- descricao produtos-->
        <div class="col-xs-12">
            <P>
                ALUTEC -DF se destaca com se
                ge a esquadrias de alumínio sob 
                te qualificados, garantindo um 
                custo reduzido.
            </P>
        </div>
        <!-- descricao produtos-->

        </div>
        <!-- dicas 06 -->
        <!--  ==============================================================  -->

        <div class="clearfix"></div>

        <!-- dicas 07 -->
        <!--  ==============================================================  -->
        <div class="col-xs-3">
          <div class="colocacao-icon">
              <a class="thumbnail top20 effect2 " href="<?php echo Util::caminho_projeto() ?>/dica">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-home01.jpg" alt="" class="input100">
            </a>
            <a class="icon-saiba" href="#">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/icon-saiba-mais.png" alt="">
            </a>
        </div>
        <!-- descricao produtos-->
        <div class="col-xs-12">
            <P>
                ALUTEC -DF se destaca com se
                ge a esquadrias de alumínio sob 
                te qualificados, garantindo um 
                custo reduzido.
            </P>
        </div>
        <!-- descricao produtos-->

        </div>
        <!-- dicas 07 -->
        <!--  ==============================================================  -->


        <!-- dicas 08 -->
        <!--  ==============================================================  -->
        <div class="col-xs-3">
          <div class="colocacao-icon">
              <a class="thumbnail top20 effect2 " href="<?php echo Util::caminho_projeto() ?>/dica">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-home01.jpg" alt="" class="input100">
            </a>
            <a class="icon-saiba" href="#">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/icon-saiba-mais.png" alt="">
            </a>
        </div>
        <!-- descricao produtos-->
        <div class="col-xs-12">
            <P>
                ALUTEC -DF se destaca com se
                ge a esquadrias de alumínio sob 
                te qualificados, garantindo um 
                custo reduzido.
            </P>
        </div>
        <!-- descricao produtos-->

        </div>
        <!-- dicas 08 -->
        <!--  ==============================================================  -->


        <!-- dicas 09 -->
        <!--  ==============================================================  -->
        <div class="col-xs-3">
          <div class="colocacao-icon">
              <a class="thumbnail top20 effect2 " href="<?php echo Util::caminho_projeto() ?>/dica">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/dicas-home01.jpg" alt="" class="input100">
            </a>
            <a class="icon-saiba" href="#">
                <img  src="<?php echo Util::caminho_projeto() ?>/imgs/icon-saiba-mais.png" alt="">
            </a>
        </div>
        <!-- descricao produtos-->
        <div class="col-xs-12">
            <P>
                ALUTEC -DF se destaca com se
                ge a esquadrias de alumínio sob 
                te qualificados, garantindo um 
                custo reduzido.
            </P>
        </div>
        <!-- descricao produtos-->

        </div>
        <!-- dicas 09 -->
        <!--  ==============================================================  -->
        <div class="clearfix"></div>

        </div>
        </div>

        <!-- DESCRICAO DICAS -->
        <!--  ==============================================================  -->



<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>


