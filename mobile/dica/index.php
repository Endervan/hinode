<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-dicas -->
  <div class="container bg-dicas">
    <div class="row"></div>
  </div>
  <!-- bg-dicas -->


  <!-- dicas gerais  -->
  <div class="container top25">
    <div class="row">
      <div class="col-xs-6">
        <h2>BSB DICAS</h2>
      </div>

      <div class="col-xs-6">
        <div class="input-group barra-pesquisa-topo">
          <input type="text" class="form-control  form1 input-lg" name="busca_topo" placeholder="ENTRE COM DICA QUE PRECISAS">
          <span class="input-group-btn">
            <button class="btn btn-default input-lg" type="submit"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </div>

    </div>
  </div>
  <!-- dicas gerais  -->


  <!-- descricao dicas dentro -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 dica-dentro">
        <h5 class="top60">20/10/2015</h5>
        <h4 class="top10">A MELHORES PRATICAS DE CONSTRUÇÃO DE PISCINAS</h4>
        <p class="top30 bottom30">Lorem Ipsum is simply dummy text of the printing and typesetting indust
          ry. Lorem Ipsumhas been the industry's standard dummy text ever since t
          he 1500s, when an unknown printer took a galley of type and scrambled 
          it to make a type specimen book. It has survived not only five centuries, b
          ut also the leap into electronic typesetting, remaining essentially unchang
          ed. It was popularised in the 1960s with the release of Letraset sheets co
          ntaining Lorem Ipsum passages, and more recently with desktop publishi
          ng software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/dicas-dentro01.jpg" alt="" >
        <p class="top20 bottom30">Lorem Ipsum is simply dummy text of the printing and typesetting indust
          ry. Lorem Ipsumhas been the industry's standard dummy text ever since t
          he 1500s, when an unknown printer took a galley of type and scrambled 
          it to make a type specimen book. It has survived not only five centuries, b
          ut also the leap into electronic typesetting, remaining essentially unchang
          ed. It was popularised in the 1960s with the release of Letraset sheets co
          ntaining Lorem Ipsum passages, and more recently with desktop publishi
          ng software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
      </div>
    </div>
  </div>
  <!-- descricao dicas dentro -->


  <!-- comentarios -->
  <div class="container bottom25">
    <div class="row comentarios">

      <div class="col-xs-6">
        <h6 class="top5">COMENTÁRIOS(3)</h6>
      </div>
      <!-- BOTAO DEIXE SEU COMENTÁRIO -->
      <div class="col-xs-6 text-right">
        <a href="">
          <button type="button" class="btn btn-primary btn-azul btn-lg">DEIXE SEU COMENTÁRIO</button>
        </a>
      </div>
      <!-- BOTAO DEIXE SEU COMENTÁRIO -->


      <!-- descricao comentario -->
      <div class="col-xs-12 top35">
        <div class="jumbotron descricao-comentario">
          <h1>MARCELO BARRETO</h1>
          <p class="top20 bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting indu
            ry. Lorem Ipsumhas been the industry's standard dummy text ever sinc
            he 1500s, when an unknown printer took a galley of type and scrambl
            it to make a type specimen book. It has survived not only five centurie
            ut also the leap into electronic typesetting, remaining essentially
          </p>

        </div>
      </div>

       <div class="col-xs-12">
        <div class="jumbotron descricao-comentario">
          <h1>MARCELO BARRETO</h1>
          <p class="top20 bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting indu
            ry. Lorem Ipsumhas been the industry's standard dummy text ever sinc
            he 1500s, when an unknown printer took a galley of type and scrambl
            it to make a type specimen book. It has survived not only five centurie
            ut also the leap into electronic typesetting, remaining essentially
          </p>

        </div>
      </div>

       <div class="col-xs-12">
        <div class="jumbotron descricao-comentario">
          <h1>MARCELO BARRETO</h1>
          <p class="top20 bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting indu
            ry. Lorem Ipsumhas been the industry's standard dummy text ever sinc
            he 1500s, when an unknown printer took a galley of type and scrambl
            it to make a type specimen book. It has survived not only five centurie
            ut also the leap into electronic typesetting, remaining essentially
          </p>

        </div>
      </div>
      <!-- descricao comentario -->

    </div>
  </div>
  <!-- comentarios -->


  <?php require_once('../includes/rodape.php'); ?>


</body>

</html>