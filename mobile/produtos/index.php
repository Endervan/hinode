<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


  <!-- FlexSlider -->
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: true,
        animationLoop: false,
        slideshow: false,
        itemWidth: 130,
        itemMargin: 5,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        customDirectionNav: $(".custom-navigation a"),
        controlNav: true,
        itemWidth: 141,
        itemMargin: 7,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>



</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-produtos -->
  <div class="container bg-produtos">
    <div class="row"></div>
  </div>
  <!-- bg-produtos -->


  <!-- produtos barra de pesquisas  -->
  <div class="container top25">
    <div class="row text-center">


      <div class="col-xs-4">
        <div class=" input-group barra-pesquisa-topo1">
          <input type="text" class="form-control fundo-form1  input-lg" name="busca_topo" placeholder="PESQUISAR PRODUTOS">
          <span class="input-group-btn">
            <button class="btn btn-default input-lg" type="submit"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </div>

      <div class="col-xs-4">
        <select class="fundo-form1 menu-home1">
          <option value="">ORDENAR POR</option>
          <option value="http://localhost/clientes/bsb/mobile">HOME</option>
          <option value="http://localhost/clientes/bsb/mobile/empresa/">A EMPRESA</option>
          <option value="http://localhost/clientes/bsb/mobile/produtos/">PRODUTOS</option>
          <option value="http://localhost/clientes/bsb/mobile/dicas/">DICAS</option>
          <option value="http://localhost/clientes/bsb/mobile/orcamentos/">ORÇAMENTO</option>
          <option value="http://localhost/clientes/bsb/mobile/fale-conosco/">FALE CONOSCO</option>
          <option value="http://localhost/clientes/bsb/mobile/portfolio/">PORTIFÓLIO</option>
        </select>
      </div>

      <div class="col-xs-4">
        <select class="fundo-form1 menu-home1">
          <option value="">ORDENAR POR</option>
          <option value="http://localhost/clientes/bsb/mobile">HOME</option>
          <option value="http://localhost/clientes/bsb/mobile/empresa/">A EMPRESA</option>
          <option value="http://localhost/clientes/bsb/mobile/produtos/">PRODUTOS</option>
          <option value="http://localhost/clientes/bsb/mobile/dicas/">DICAS</option>
          <option value="http://localhost/clientes/bsb/mobile/orcamentos/">ORÇAMENTO</option>
          <option value="http://localhost/clientes/bsb/mobile/fale-conosco/">FALE CONOSCO</option>
          <option value="http://localhost/clientes/bsb/mobile/portfolio/">PORTIFÓLIO</option>
        </select>
      </div>

    </div>
  </div>
  <!-- produtos barra de pesquisas  -->


  <!-- flex slider -->
  <div class="container top10">
    <div class="row slider-prod1">
      <div class="col-xs-12">

        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider text-center">
          <ul class="slides lista-categorias">

            <li>
              <a href="" >
               <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
               <h5>PISCINAS</h5> 
             </a>
           </li>

           <li >
            <a href="" >
             <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
             <h5>PISCINAS</h5> 
           </a>
         </li>


           <li>
            <a href="" >
             <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
             <h5>PISCINAS</h5> 
           </a>
         </li>

           <li>
            <a href="" >
             <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
             <h5>PISCINAS</h5> 
           </a>
         </li>

         <li>
          <a href="" >
           <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
           <h5>PISCINAS</h5> 
         </a>
       </li>

         <li>
          <a href="" >
           <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
           <h5>PISCINAS</h5> 
         </a>
       </li>

       <li>
        <a href="" >
         <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
         <h5>PISCINAS</h5> 
       </a>
      </li>

      <li>
        <a href="" >
         <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/exemplo-categoria.png" alt="">
         <h5>PISCINAS</h5> 
       </a>
      </li>

<!-- items mirrored twice, total of 12 -->
</ul>


</div>
<!-- seta personalizadas -->
<div class="custom-navigation text-center">
  <a href="#" class="flex-prev"><i class="fa fa-angle-left"></i>
  </a>


  <a href="#" class="flex-next"><i class="fa fa-angle-right"></i>
  </a>

</div>
<!-- seta personalizadas -->

</div>
</div>
</div>


<!-- produtos home  -->
<div class="container">
  <div class="row">

    <!-- produto 01 -->
    <div class="col-xs-6 top20">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home01.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>

    <!-- produto 02 -->
    <div class="col-xs-6 top20">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home02.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>

    <!-- produto 03 -->
    <div class="col-xs-6 top15">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home01.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>


    <!-- produto 04 -->
    <div class="col-xs-6 top15">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home02.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>

    <!-- produto 05 -->
    <div class="col-xs-6 top15">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home02.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>

    <!-- produto 06 -->
    <div class="col-xs-6 top15">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home02.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>


    <!-- produto 07 -->
    <div class="col-xs-6 top15">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home02.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>


    <!-- produto 08 -->
    <div class="col-xs-6 top15">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home02.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>



    <!-- botao produto -->
    <div class="col-xs-12 text-center top25 bottom40">
      <a href="#" class="btn btn-primary" role="button">VER PRODUTOS</a>
    </div>
    <!-- botao produto -->


  </div>
</div>
<!-- produtos home  -->






<?php require_once('../includes/rodape.php'); ?>


</body>

</html>