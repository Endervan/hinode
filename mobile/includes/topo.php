<div class="container topo">
    <div class="row  top10">
        <!-- logo -->

        <div class="col-xs-5 ">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="Home">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home">
            </a>
        </div> 
        <!-- logo -->  

        <!-- contatos topo  -->
        <div class="col-xs-7 telefone-topo text-right">
            <h4>
                (61) 3552-1121
                <a href="tel:6135249876" class="btn btn-azul">
                    CHAMAR
                </a>

            </h4>

            <h4 class="top5">
                (61) 3552-1121
                <a href="tel:6135249876" class="btn btn-azul">
                    CHAMAR
                </a>

            </h4>
            
            
            <div class=" col-xs-11 pull-right input-group barra-pesquisa-topo1 top10 ">
                <input type="text" class="form-control form  input-lg" name="busca_topo" placeholder="PESQUISAR PRODUTOS">
                <span class="input-group-btn">
                    <button class="btn btn-default input-lg" type="submit"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>

        </div>
        <!-- contatos topo  -->



        <div class=" col-xs-12 effect2 top25">

            <div class="menu-topo ">
                <!-- menu -->
                <div class="col-xs-6 ">
                    <select class="menu-home" id="menu-site">
                        <option value=""></option>
                        <option value="<?php echo Util::caminho_projeto() ?>/mobile">HOME</option>
                        <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa/">A EMPRESA</option>
                        <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos/">PRODUTOS</option>
                        <option value="<?php echo Util::caminho_projeto() ?>/mobile/dicas/">DICAS</option>
                        <option value="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/">ORÇAMENTO</option>
                        <option value="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco/">FALE CONOSCO</option>
                        <option value="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco/">TRABALHE CONOSCO</option>
                        <option value="<?php echo Util::caminho_projeto() ?>/mobile/servicos/">SERVICOS</option>
                    </select>
                </div>
                <!-- menu -->

                <!-- orcamento topo -->
                <div class="col-xs-6 ">
            
                        <div class="dropdown">
                              <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-azul">
                                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bg-menu-topo.png" alt="">
                            </a>
                            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

                                <h6 class="bottom20">MEU ORÇAMENTO(<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h6>

                               <?php
                                if(count($_SESSION[solicitacoes_produtos]) > 0)
                                {
                                    for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                                    {
                                        $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                                        ?>
                                        <div class="lista-itens-carrinho">
                                            <div class="col-xs-2">
                                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">    
                                            </div>
                                            <div class="col-xs-8">
                                                <h1><?php Util::imprime($row[titulo]) ?></h1>
                                            </div>
                                            <div class="col-xs-1">
                                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                                            </div>
                                        </div>
                                        <?php  
                                    }
                                }
                                ?>






                               

                                <div class="text-right bottom20">
                                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos" title="Finalizar" class="btn btn-primary" >
                                        FINALIZAR
                                    </a>
                                </div>


                            </div>

                        </div>

            
            </div>
            <!-- orcamento topo -->

        </div>
    </div>

</div>

</div>







<!-- <div class="col-xs-6">
            
        </div> -->