<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-orcamentos -->
  <div class="container bg-orcamentos">
    <div class="row"></div>
  </div>
  <!-- bg-orcamentos -->


  <!-- orcamentos gerais  -->
  <div class="container top25">
    <div class="row orcamentos">
      <div class="col-xs-12">
        <h2>PRODUTOS SELECIONADOS</h2>
      </div>

      <!-- itens e descricao carrinho -->
      <div class="col-xs-12">
        <table class="table top10 tb-lista-itens">
          <thead>
            <tr>
              <th class="text-center">ITEM</th>
              <th>DESCRIÇÃO</th>
              <th class="text-center">QDT.</th>
              <th class="text-center">EXCLUIR</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/itens-orcamento.jpg" alt="Home"></td>
              <td>CLORO PUTO PARA PISCINAS E ÁGUA POTÁVEL</td>
              <td class="text-center">
                <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                <input name="idproduto[]" type="hidden" value="262"  />
              </td>
              <td class="text-center">
                <a href="?action=del&id=0&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> 
                  <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/retira-orcamento.jpg" alt="">
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- itens e descricao carrinho -->

    </div>
  </div>
  <!-- orcamentos gerais  -->


  <!-- FORMULARIO -->
  <div class="container top40">
    <div class="row orcamentos">
      <div class="col-xs-12">
        <h2>CONFIRME SEUS DADOS</h2>  
      </div>


      <!-- formulario orcamento -->
      <div class="FormContato col-xs-12 top20 bottom25">
        <div class="row">
          <div class="col-xs-6 form-group ">
            <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
            <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
          </div>

          <div class="col-xs-6 form-group ">
            <label class="glyphicon glyphicon-user"> <span>E-mail</span></label>
            <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
          </div>
        </div>

        <div class="row">
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
            <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
          </div>


          <div class="col-xs-6 form-group">
            <label <i class="fa fa-mobile"></i> <span>Celular</span></label>
            <input type="text" name="celular" class="form-control fundo-form1 input100" placeholder="">
          </div>


        </div>

        <div class="row">
         <div class="col-xs-6 form-group">
           <label <i class="fa fa-home"></i> <span>Bairro</span></label>
           <input type="text" name="bairro" class="form-control fundo-form1 input100" placeholder="">
         </div>


         <div class="col-xs-6 form-group">
          <label <i class="fa fa-globe"></i> <span>Cidade</span></label>
          <input type="text" name="cidade" class="form-control fundo-form1 input100" placeholder="">
        </div>
      </div>

      <div class="row">
       <div class="col-xs-6 form-group">
        <label <i class="fa fa-globe"></i><span>Estado</span></label>
        <input type="text" name="estado" class="form-control fundo-form1 input100" placeholder="">
      </div>

      <div class="col-xs-6 form-group">
        <label class="glyphicon glyphicon-star"> <span>Tipo de pessoa</span></label>
        <input type="text" name="tipo_pessoa" class="form-control fundo-form1 input100" placeholder="">
      </div>

    </div>

    <div class="row">
      <div class="col-xs-12 form-group">
        <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
        <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="text-right  top30">
      <button type="submit" class="btn btn-azul" name="btn_contato">
        ENVIAR
      </button>
    </div>
  </div>
  <!-- formulario orcamento -->


  <!-- pesquisas orcamentos -->
  <div class="col-xs-10 pesquisa-orcamento bottom20">
    <p>Tem interesse em receber orçamento de outros produtos:</p>

    <label class="radio-inline">
      <input type="radio" name="receber_orcamento" value="SIM"> SIM
    </label>
    <label class="radio-inline">
      <input type="radio" name="receber_orcamento"  value="NÃO"> NÃO
    </label>
    <p>Se sim, assinale abaixo</p>

    <div class="checkbox col-xs-12">
      <label>
        <input type="checkbox" name="categorias[]" value="PISCINAS EM VINIL">
        PISCINAS EM VINIL                  
      </label>
    </div>
    <div class="checkbox col-xs-12">
      <label>
        <input type="checkbox" name="categorias[]" value="AQUECEDOR PARA PISCINAS">
        AQUECEDOR PARA PISCINAS                  
      </label>
    </div>
    <div class="checkbox col-xs-12">
      <label>
        <input type="checkbox" name="categorias[]" value="SAUNA E VAPOR">
        SAUNA E VAPOR                  
      </label>
    </div>
    <div class="checkbox col-xs-12">
      <label>
        <input type="checkbox" name="categorias[]" value="FILTROS E BOMBAS">
        FILTROS E BOMBAS                  
      </label>
    </div>

    <div class="top10 row"></div>

    <p>Como você conheceu o site da BSB?</p>

    <select class="form-control input-lg" name="como_conheceu">
      <option value="">Selecione</option>
      <option>Jornais</option>
      <option>Revistas</option>
      <option>Sites</option>
      <option>Fórum</option>
      <option>Notícias</option>
    </select>
  </div>
  <!-- pesquisas orcamentos -->


</div>
</div>




<?php require_once('../includes/rodape.php'); ?>


</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      celular: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipo_pessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
