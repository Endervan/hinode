<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>

  <!-- FlexSlider -->
  <script defer src="http://masmidia.com.br/clientes/san-remo/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    $(window).load(function() {
          // The slider being synced must be initialized first
          $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 127,
            itemMargin:7,
            asNavFor: '#slider'
          });

          $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
          });
        });
  </script>

  


  
</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-produtos -->
  <div class="container bg-produtos">
    <div class="row"></div>
  </div>
  <!-- bg-produtos -->


  <!-- produtos barra de pesquisas  -->
  <div class="container top25">
    <div class="row text-center">


      <div class="col-xs-4">
        <div class=" input-group barra-pesquisa-topo1">
          <input type="text" class="form-control fundo-form1  input-lg" name="busca_topo" placeholder="PESQUISAR PRODUTOS">
          <span class="input-group-btn">
            <button class="btn btn-default input-lg" type="submit"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </div>

      <div class="col-xs-4">
        <select class="fundo-form1 menu-home1">
          <option value="">ORDENAR POR</option>
          <option value="http://localhost/clientes/bsb/mobile">HOME</option>
          <option value="http://localhost/clientes/bsb/mobile/empresa/">A EMPRESA</option>
          <option value="http://localhost/clientes/bsb/mobile/produtos/">PRODUTOS</option>
          <option value="http://localhost/clientes/bsb/mobile/dicas/">DICAS</option>
          <option value="http://localhost/clientes/bsb/mobile/orcamentos/">ORÇAMENTO</option>
          <option value="http://localhost/clientes/bsb/mobile/fale-conosco/">FALE CONOSCO</option>
          <option value="http://localhost/clientes/bsb/mobile/portfolio/">PORTIFÓLIO</option>
        </select>
      </div>

      <div class="col-xs-4">
        <select class="fundo-form1 menu-home1">
          <option value="">ORDENAR POR</option>
          <option value="http://localhost/clientes/bsb/mobile">HOME</option>
          <option value="http://localhost/clientes/bsb/mobile/empresa/">A EMPRESA</option>
          <option value="http://localhost/clientes/bsb/mobile/produtos/">PRODUTOS</option>
          <option value="http://localhost/clientes/bsb/mobile/dicas/">DICAS</option>
          <option value="http://localhost/clientes/bsb/mobile/orcamentos/">ORÇAMENTO</option>
          <option value="http://localhost/clientes/bsb/mobile/fale-conosco/">FALE CONOSCO</option>
          <option value="http://localhost/clientes/bsb/mobile/portfolio/">PORTIFÓLIO</option>
        </select>
      </div>

    </div>
  </div>
  <!-- produtos barra de pesquisas  -->



  <div class="container top20">
    <div class="row">

      <!-- botao voltar -->
      <div class="col-xs-6 descricao-produtos">

        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn-primary btn-voltar" role="button"><i class="fa fa-angle-double-left"></i>VOLTAR</a>
        <!-- botao voltar -->
      </div>


      <!-- atendimento -->
      <div class="col-xs-6 topo-telefone1 text-right">
       <h6>ATENDIMENTO EM</h6>
       <div class="telefone-topo1 top5">
        <h4>
          (61) 3552-1121
          <a href="tel:6135249876" class="btn btn-azul">
            CHAMAR
          </a>

        </h4>

        <h4 class="top5">
          (61) 3552-1121
          <a href="tel:6135249876" class="btn btn-azul">
            CHAMAR
          </a>

        </h4>


      </div>

    </div>
    <!-- atendimento -->

  </div>
</div>





<!-- flex slider -->
<div class="container">
  <div class="row slider-prod-dentro">


    <!-- descricao do produto -->
    <div class="col-xs-12 slider-produtos">
      <h4>PISCINAS EM VINIL</h4>
      <!-- Place somewhere in the <body> of your page -->
      <div id="slider" class="flexslider">
        <ul class="slides slider-prod">

          <li>
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas.jpg" alt="">
          </li>

          <li>
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas.jpg" alt="">
          </li>

          <li>
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas.jpg" alt="">
          </li>

          <li>
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas.jpg" alt="">
          </li>

          <li>
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas.jpg" alt="">
          </li>


          <li>
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas.jpg" alt="">
          </li>


          <!-- items mirrored twice, total of 12 -->
        </ul>
      </div>


      <div class="slider-prod-dentro11">
        <h4>SELECIONE O REVESTIMENTO</h4>
        <div id="carousel" class="flexslider">

          <ul class="slides slider-prod-tumb">

            <li>
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas-thumb.jpg" alt="">
            </li>

            <li>
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas-thumb.jpg" alt="">
            </li>

            <li>
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas-thumb.jpg" alt="">
            </li>

            <li>
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas-thumb.jpg" alt="">
            </li>

            <li>
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas-thumb.jpg" alt="">
            </li>

            <li>
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produtos-dentro-piscinas-thumb.jpg" alt="">
            </li>


            <!-- items mirrored twice, total of 12 -->
          </ul>
        </div>       
      </div>
      <!-- BOTAO CARRINHO -->
     <div class="pull-right">
        <a href="">
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn-carinho.png" alt="">
      </a>
     </div>
      <!-- BOTAO CARRINHO -->

    </div>
  </div>
</div>




<!-- descricao produto dentro -->
<div class="container top10">
  <div class="row">
    <div class="col-xs-12">
      <h6>DESCRIÇÃO</h6>

      <p class="top15 bottom35">Lorem Ipsum is simply dummy text of the printing and typesetting indus
        try. Lorem Ipsum has been the industry's standard dummy text ever sinc
        e the 1500s, when an unknown printer took a galley of type and scramb
        led it to make a type specimen book. It has survived not only five centur
        ies, but also the leap into electronic typesetting, remaining essentially un
        changed. It was popularised in the 1960s with the release of Letraset sh
        eets containing LoremIpsum passages, and more recently with desktop 
        publishing software like Aldus PageMaker including versions of Lorem Ip
        sum.
        
      </p>

      <h6 class="bottom20">COMENTÁRIOS</h6>

    </div>
  </div>
</div>
<!-- descricao produto dentro -->


<!-- comentarios produtos dentro -->
<div class="container">
  <div class="row">


    <div class="col-xs-12 descricao-comentario1">
      <div class="jumbotron">
        <h4>Marcio</h4>
        <!-- avaliacao -->
        <div class=" text-left">
          <input id="avaliaca-1" class="avaliacao" type="number" value="4" />
        </div>
        <!-- avaliacao -->
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting 
          industry. Lorem Ipsum has been the industry's standard dummy te
          xtever since the 1500s, when an unknown printer took a galley of 
          type and scrambled it to make a type specimen bo
        </p>
      </div>
    </div>


    <div class="col-xs-12 descricao-comentario1">
      <div class="jumbotron">
        <h4>Marcio</h4>
        <!-- avaliacao -->
        <div class=" text-left">
          <input id="avaliaca-1" class="avaliacao" type="number" value="4" />
        </div>
        <!-- avaliacao -->
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting 
          industry. Lorem Ipsum has been the industry's standard dummy te
          xtever since the 1500s, when an unknown printer took a galley of 
          type and scrambled it to make a type specimen bo
        </p>
      </div>
    </div>

  </div>
</div>
<!-- comentarios produtos dentro -->


<!-- veja tambem -->
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h6 class="top15">VEJA TAMBÉM</h6>
    </div>

    <!-- produto 01 -->
    <div class="col-xs-6 top20">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home01.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>

    <!-- produto 02 -->
    <div class="col-xs-6 top20">
      <div class="thumbnail produtos-home ">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/produto-home02.jpg" alt="" class="input100">
        </a>
        <div class="caption">
          <h1>Piscinas de Azulejo e Pastilhas</h1>
          <h3 class="top10"><i class="fa fa-star"></i>EM PISCINAS EM VINIL</h3>
          <h3 class="top5"><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
        </div>
      </div>
    </div>



  </div>
</div>
<!-- veja tambem -->






<?php require_once('../includes/rodape.php'); ?>


</body>

</html>