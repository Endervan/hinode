<div class="container">
	<div class="row">
		<div class="col-xs-2 col-xs-offset-7 ">
			<ul class="ds-btn">
				<li>
					<a class="btn btn-topo text-right" href="">
						<i class="glyphicon glyphicon-earphone"></i><span>(61) 3302-4517<br></span></a> 
					</li>

				</ul>
			</div>
			<!-- carrinho topo -->
			<div class="col-xs-3 personalizar-carrinho text-right">
				<div class="dropdown">

					<a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-meu-orcamento">
						<h4><i class="glyphicon glyphicon-star"></i>FAÇA SEU ORÇAMENTO</h4>		
					</a>

					<div class="dropdown-menu topo-meu-orcamento " aria-labelledby="dLabel">

						<h6 class="bottom20 pull-right">MEU ORÇAMENTO</h6>


						<div class="lista-itens-carrinho">
							<div class="col-xs-2">
								<img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
							</div>
							<div class="col-xs-8">
								<h1></h1>
							</div>
							<div class="col-xs-1">
								<a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
							</div>
						</div>
						


						<div class="text-right bottom20">
							<a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="Finalizar" class="btn btn-amarelo">
								FINALIZAR
							</a>
						</div>


					</div>

				</div>
			</div>
			<!-- carrinho topo -->
		</div>
	</div>



	<div class="container">
		<div class="row">
			<!-- logo -->
			<div class="col-xs-3">
				<a href="<?php echo Util::caminho_projeto() ?>">
					<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
				</a>
			</div>
			<!-- logo -->


			<!-- menu topo -->
			<div class="col-xs-9 navs-personalizados top10">
				<!-- menu -->
				<nav class="navbar navbar-default navbar-right" role="navigation">
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav">
							<li class="active">
								<a href="<?php echo Util::caminho_projeto() ?>">
									HOME
								</a>
							</li>

							<li>
								<a href="<?php echo Util::caminho_projeto() ?>/empresa">
									
									A EMPRESA
								</a>
							</li>

							<li>
								<a href="<?php echo Util::caminho_projeto() ?>/produtos">
									
									PRODUTOS
								</a>
							</li>

							<li>
								<a href="<?php echo Util::caminho_projeto() ?>/servicos">
									
									SERVIÇOS
								</a>
							</li>

							
							<li>
								<a href="<?php echo Util::caminho_projeto() ?>/contatos">
									
									CONTATOS
								</a>
							</li>

						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</nav>    
			</div>
			<!-- menu topo-->  
		</div>
	</div>




<!-- menu-topo


