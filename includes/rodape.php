<div class="container-fluid rodape-preto">
	<div class="row">

		<!-- menu -->
		<div class="container top40">
			<div class="row">
				<div class="col-xs-12">
					<ul class="menu-rodape">
						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
						</li>
						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a>
						</li>
						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
						</li>
						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/portifolio">PORTIFOLIO</a>
						</li>
						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a>
						</li>
						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/fornecedores">FORNECEDORES</a>
						</li>
						
						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSO</a>
						</li>

					</ul>
				</div>
			</div>
		</div>
		<!-- menu -->

		<!-- logo, endereco, telefone -->
		<div class="container bottom20 top20">
			<div class="row">
				
				<div class="col-xs-10">
					<div class="contatos-rodape top10">
						
						<span><i class="glyphicon glyphicon-home"></i>SSTN Edifício Montreal 3 Sala 02 Asa Norte Brasília – DF</span>
						<h2 class="top5"><i class="glyphicon glyphicon-earphone"></i>(61) 3322-1144 / (61) 3322-1145</h2>
					</div>
			
				</div>
				<div class="col-xs-2 text-right">
					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png"  alt="">
					</a>
				</div>
			</div>
		</div>
		<!-- logo, endereco, telefone -->

	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="rodape-azul">
			<h5 class="text-center">TODOS OS DIREITOS RESERVADOS</h5>
		</div>
	</div>
</div>



<script type="text/javascript">
	$(document).ready(function() {

		$(".produto-hover").hover(
			function () {
				$(this).stop().animate({opacity:1});
			},
			function () {
				$(this).stop().animate({opacity:0});
			}
			);

	});
</script>
